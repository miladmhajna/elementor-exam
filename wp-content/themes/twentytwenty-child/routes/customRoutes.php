<?php


// testing: http://elementor-exam.test/wp-json/twentytwentyone-child/v1/latest-posts/5
class Custom_Routes {

  private $slug = 'twentytwentyone-child';
  private $version = 'v1';
  
  public function __construct() {
    add_action('rest_api_init', [$this, 'register_routes']);
  }

  public function register_routes() {
    register_rest_route( $this->slug . '/' . $this->version, 'latest-posts/(?P<custom_category_id>\d+)',array(
      'methods'  => 'GET',
      'callback' => [$this, 'get_latest_posts_by_category'],
      'permission_callback' => '__return_true'
    ));
  }

  function get_latest_posts_by_category($request) {
  
    $args = array(
      'post_type'=> 'products',
      'tax_query' => array(
        array(
          'taxonomy' => 'custom-categories',
          'field' => 'term_id',
          'terms' => $request['custom_category_id']
        )
      )
    );
  
    $posts = get_posts($args);
  
    if (empty($posts))
      return new WP_Error( 'empty_category', 'There are no posts to display', array('status' => 404) );
  
    $posts = array_map(function($post) {
      return array(
        'title' => get_the_title($post->ID),
        'description' => apply_filters('the_content', $post->post_content),
        'image' => get_the_post_thumbnail_url($post->ID),
        'price' => get_post_meta($post->ID, 'product_price', true),
        'is_on_sale' => get_post_meta($post->ID, 'product_on_sale', true),
        'sale_price' => get_post_meta($post->ID, 'product_sale_price', true),
      );
    }, $posts);

    $response = new WP_REST_Response($posts);
    $response->set_status(200);
  
    return $response;
  }
}
<?php

  // Custom address bar for mobile
  add_action('wp_head', 'add_meta_tag');
  function add_meta_tag() {
    if(wp_is_mobile()) {
      echo '<meta name="theme-color" content="#fed322" />';	
    }
  }


  // disable admin bar for user role editor
	add_action('after_setup_theme', 'remove_admin_bar');

	function remove_admin_bar() {
		if (!current_user_can('editor') && !is_admin()) {
			show_admin_bar(false);
		}
	}

  function create_custom_tax_query($post_id, $taxonomy) {
    $custom_terms = wp_get_post_terms($post_id, $taxonomy);

    if( $custom_terms ){

      // going to hold our tax_query params
      $tax_query = array();

      // add the relation parameter (not sure if it causes trouble if only 1 term so what the heck)
      if( count( $custom_terms ) > 1 )
          $tax_query['relation'] = 'OR' ;

      // loop through venus and build a tax query
      foreach( $custom_terms as $custom_term ) {

          $tax_query[] = array(
              'taxonomy' => $taxonomy,
              'field' => 'slug',
              'terms' => $custom_term->slug,
          );

      }

      return $tax_query;
    }

    return [];

  }


?>
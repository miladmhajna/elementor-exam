<?php
/**
 * The template for displaying the 404 template in the Twenty Twenty theme.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();

$on_sale = get_post_meta($post->ID, 'product_on_sale', true);

$price = get_post_meta($post->ID, 'product_price', true);
$price = $price ? number_format((float)$price, 2) : null;

$sale_price = get_post_meta($post->ID, 'product_sale_price', true);
$sale_price = $sale_price ? number_format((float)$sale_price, 2) : null;

$youtube_url = get_post_meta($post->ID, 'product_youtube_video', true);
$gallery_ids = get_post_meta($post->ID, 'product_gallery', true);

?>

<main id="site-content" role="main">

	<div class="section-inner">

		<h1 class="entry-title md-none title-head"><?php the_title(); ?></h1>


		<div class="single-item">


			<div class="info">
				<h2 class="entry-title"><?php the_title(); ?></h2>

				<p class="price">
					<?php _e( 'Price', 'twentytwenty' ) ?>:
					<?php if($sale_price): ?>
						<span class="price-old"><?php echo $price ?></span>
						<span><?php echo $sale_price ?></span>
					<?php else: ?>
						<span><?php echo $price ?></span>
					<?php endif; ?>
				</p>
				
				<?php the_content();?>

				<?php if($youtube_url) 
						echo "<iframe src=". $youtube_url ." class='youtube_url' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
				?>
				
			</div>

			<div class="gallery">
				<?php if($on_sale == 'yes') 
					echo "<span class='badge badge-sale'>Sale</span>";
				?>
				<div class="list">
				<?php

					if ( ! empty( $gallery_ids ) ) :
						$image_ids = explode( ',', $gallery_ids );
						foreach($image_ids as $image_id) {
								$image_url = wp_get_attachment_url($image_id); ?>
								<div class="gallery-item">
										<img src="<?php echo $image_url; ?>" />
								</div>
								<?php
						}
					endif;
					?>
				</div>
			</div>

		</div>

		<?php get_template_part( 'template-parts/related-items', null, [
			'post_type' => 'products',
			'per_page' => 4,
			'tax_query' => create_custom_tax_query($post->ID, 'custom-categories'),
      'post__not_in'   => [$post->ID],
  		'post_status' => 'publish',
			'orderby' => 'title', 
			'order' => 'ASC',
		] ); ?>

	</div><!-- .section-inner -->

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>

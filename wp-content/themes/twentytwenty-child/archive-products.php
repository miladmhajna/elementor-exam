<?php
/**
 * Template Name: Archive Products
 * Template Post Type: products, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

$args = array(  
  'post_type' => 'products',
  'post_status' => 'publish',
  'posts_per_page' => -1, 
  'orderby' => 'title', 
  'order' => 'ASC',
);

$loop = new WP_Query( $args ); 

get_header();
?>

<main id="site-content" role="main">





<div class="section-inner">

    <h1><?php _e( 'Products', 'twentytwenty' ) ?></h1>

    <h5><small><?php _e( 'Shortcode', 'twentytwenty' ) ?></small></h5>
    <?php echo do_shortcode('[product-item product_id="86" bg_color="#fed322"]') ?>

    
    <div class="items-grid">
    <?php
    if ( $loop->have_posts() ) :
    while ( $loop->have_posts() ) : $loop->the_post(); 

      ?>
      <a class="item" href="<?php the_permalink($post->ID) ?>" style="background-image: url(<?php the_post_thumbnail_url($post->ID) ?>)">

        <?php if(get_post_meta($post->ID, 'product_on_sale', true) == 'yes') 
          echo "<span class='badge badge-sale'>Sale</span>";
        ?>

        <h3 class="item-title"><?php the_title(); ?></h3>
      </a>
      <?php
    endwhile;
    else :
    ?> <h3><?php _e( 'No Products found!', 'twentytwenty' ) ?></h3> <?php
    endif;
    wp_reset_postdata();
    ?>
    </div>
</div>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>

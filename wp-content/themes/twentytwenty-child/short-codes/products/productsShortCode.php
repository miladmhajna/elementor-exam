<?php


class Products_Short_Code {
  private $short_code;
  private $post_type;

  public function __construct($post_type, $short_code) {
    $this->post_type = $post_type;
    $this->short_code = $short_code;

    add_shortcode($this->short_code, [$this, 'create_short_code'] );
    add_filter($this->short_code, [$this, 'filter_short_code']);
  }

  public function create_short_code($atts = []) {

    $args = shortcode_atts([
      'product_id' => null,
      'bg_color' => '#fff'
    ], $atts );



    $query = new WP_Query([
      'p'           => $args['product_id'],
      'post_type'   => $this->post_type,
      'post_status' => 'publish'
    ]);

    ob_start();

    if($query->have_posts()) :

      $post = get_post($args['product_id']);
      
      $price = get_post_meta($post->ID, 'product_price', true);
      $sale_price = get_post_meta($post->ID, 'product_sale_price', true);

      $final_price = !empty($sale_price) ? $sale_price : $price;
      $final_price = number_format((float)$final_price, 2);

      ?>
        <a class="product-item" 
          href="<?php the_permalink($post->ID) ?>"
          style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID) ?>);">
          <span class="badge badge-price" style="background-color: <?php echo $args['bg_color'] ?>;"><?php echo $final_price ?></span>
          <h3 class="item-title"><?php echo get_the_title($post->ID) ?></h3>
        </a>
      <?php
  

  
    endif;    

    $result = ob_get_contents();

    ob_end_clean();

    return apply_filters($this->short_code, $result);

  }

  public function filter_short_code($input) {
    return $input;
  }

}
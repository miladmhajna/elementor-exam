<?php


require_once(dirname(__FILE__) . '/../../fields/products/productsFields.php');
require_once(dirname(__FILE__) . '/../../short-codes/products/productsShortCode.php');

Class Product_Post_Type {

  private $name = 'Products';
  private $singular_name = 'Product';
  private $slug = 'products';
  private $short_code = 'product-item';

  public function __construct() {

    add_action( 'init', [$this, 'create_posttype'] );

    new Products_fields($this->slug);
    new Products_Short_Code($this->slug, $this->short_code);


    add_action( 'wp_enqueue_scripts', [$this, 'post_type_scripts'] );

  }

  public function post_type_scripts() {
    wp_register_style('items-grid-style',get_template_directory_uri().'/assets/css/items-grid-style.css');
    wp_enqueue_style('items-grid-style');
  }

  public function create_posttype() {

    // Set UI labels for Custom Post Type
    $labels = array(
      'name' => __( $this->name ),
      'singular_name' => __( $this->name )
    );
    
    
    // Set other options for Custom Post Type
    
    $args = array(
      'label'               => __( $this->name ),
      'labels'              => $labels,
      // Features this CPT supports in Post Editor
      'supports'            => array(
        'title',        // Post title
        'editor',       // Post content
        'excerpt',      // Allows short description
        'thumbnail',    // Allows feature images
        'revisions',    // Shows autosaved version of the posts
        'custom-fields' // Supports by custom fields
      ),
      // You can associate this CPT with a taxonomy or custom taxonomy. 
      'taxonomies'          => array( 'custom-categories' ),
      'public'              => true,
      'menu_position'       => 5,
      'has_archive'         => true,
      'rewrite' => array('slug' => $this->slug),
      'show_in_rest' => true,
    
    );
    
    // Registering your Custom Post Type
    register_post_type( $this->slug, $args );
    
  }

}

?>
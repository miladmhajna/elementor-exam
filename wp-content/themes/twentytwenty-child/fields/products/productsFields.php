<?php

class Products_fields {

  private $slug;

  private $nonce_name = 'product_fields';

  public function __construct($slug) {

    $this->slug = $slug;

    // Hooking up our function to theme setup
    add_action( 'admin_enqueue_scripts',[$this, 'product_script'] );

    add_action( 'add_meta_boxes', [$this, 'create_meta_boxes'] );
    add_action( 'save_post', [$this, 'save_meta_boxes'], 1, 2 );

  }

  public function product_script() {
    wp_enqueue_script( 'mytheme-gallery-js', get_template_directory_uri() . '/fields/products/custom-gallery.js', array('jquery'), null, true );
  }


  public function create_meta_boxes() {

    add_meta_box(
      'product_all_fields',
      'Product Details',
      [$this, 'product_details_html'],
      $this->slug,
      'normal',
      'default'
    );

    add_meta_box(
      'product_gallery',
      'Product Gallery',
      [$this, 'product_gallery_html'],
      $this->slug,
      'normal',
      'default'
    );

  }

  public function product_gallery_html() {

    global $post;

    wp_nonce_field( basename( __FILE__ ), $this->nonce_name );

    $meta_key = 'product_gallery';
    $image = 'Upload Image';
    $button = 'button';
    $image_size = 'full'; // it would be better to use thumbnail size here (150x150 or so)
    $display = 'none'; // display state of the "Remove image" button
    
    $value = get_post_meta($post->ID, $meta_key, true);

    ?>
        
      <p><?php
          _e( '<i>Set Images for Featured Image Gallery</i>' );
      ?></p>
      
      <label>
        <div class="gallery-screenshot clearfix">
            <?php
            {
                $ids = explode(',', $value);
                foreach ($ids as $attachment_id) {
                    $img = wp_get_attachment_image_src($attachment_id, 'thumbnail');
                    echo '<div class="screen-thumb"><img src="' . esc_url($img[0]) . '" /></div>';
                }
            }
            ?>
        </div>
        
        <input id="edit-gallery" class="button upload_gallery_button" type="button"
              value="<?php echo __('Add/Edit Gallery') ?>"/>
        <input id="clear-gallery" class="button upload_gallery_button" type="button"
              value="<?php echo __('Clear') ?>"/>
        <input type="hidden" name="<?php echo esc_attr($meta_key); ?>" id="<?php echo esc_attr($meta_key); ?>" class="gallery_values" value="<?php echo esc_attr($value); ?>">
      </label>
    <?php  
  }

  public function product_details_html() {
    global $post;

    // Nonce field to validate form request came from current site
    wp_nonce_field( basename( __FILE__ ), $this->nonce_name );

    $price = get_post_meta( $post->ID, 'product_price', true);
    $sale_price = get_post_meta( $post->ID, 'product_sale_price', true);
    $on_sale = get_post_meta( $post->ID, 'product_on_sale', true);
    $youtube_url = get_post_meta( $post->ID, 'product_youtube_video', true);

    ?>

    <div class="row">
      <div class="label">Product price</div>
      <div class="fields"><input type="number" min="0" name="product_price" value="<?php echo $price; ?>"</div>
    </div>
    
    <br/>

    <div class="row">
      <div class="label">Product sale price</div>
      <div class="fields"><input type="number" min="0" name="product_sale_price" value="<?php echo $sale_price; ?>"</div>
    </div>
    
    <br/>
    
    <div class="row">
        <div class="label">Product on sale ?</div>
        <div class="fields">
            <label><input type="checkbox" name="product_on_sale" value="yes" <?php if($on_sale == 'yes') echo 'checked'; ?> />On sale</label>
        </div>
    </div>
    
    <br/>
    
    <div class="row">
      <div class="label">Product Youtube video url</div>
      <div class="fields"><input type="text" name="product_youtube_video" value="<?php echo $youtube_url; ?>"</div>
    </div>
    
    <?php
    

  }

  function save_meta_boxes( $post_id, $post ) {

    // Return if the user doesn't have edit permissions.
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
      return $post_id;
    }

    // Verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times.
    if (isset($_POST[$this->nonce_name]) && ! wp_verify_nonce( $_POST[$this->nonce_name], basename(__FILE__) ) ) {
      return $post_id;
    }

    // Now that we're authenticated, time to save the data.
    // This sanitizes the data from the field and saves it into an array $events_meta.
    $events_meta = [
      'product_price' => $_POST['product_price'] ?? null,
      'product_sale_price' => $_POST['product_sale_price'] ?? null,
      'product_on_sale' => $_POST['product_on_sale'] ?? 'no',
      'product_youtube_video' => $_POST['product_youtube_video'] ?? '',
      'product_gallery' => $_POST['product_gallery'] ?? []
    ];

    // Cycle through the $events_meta array.
    // Note, in this example we just have one item, but this is helpful if you have multiple.
    foreach ( $events_meta as $key => $value ) :

      // Don't store custom data twice
      if ( 'revision' === $post->post_type ) {
        return;
      }

      if ( get_post_meta( $post_id, $key, false ) ) {
        // If the custom field already has a value, update it.
        update_post_meta( $post_id, $key, $value );
      } else {
        // If the custom field doesn't have a value, add it.
        add_post_meta( $post_id, $key, $value);
      }

      if ( ! $value ) {
        // Delete the meta key if there's no value
        delete_post_meta( $post_id, $key );
      }

    endforeach;

  }
}
<?php


class Custom_Taxonomy {

  private $data = [];
  
  public function __construct($data) {
    $this->data = $data;
    add_action( 'init', [$this, 'create_taxonomy'], 0 );
  }

  public function create_taxonomy() {
    // Add new taxonomy, make it hierarchical like categories
    //first do the translations part for GUI
    
    $labels = array(
      'name' => __( $this->data['name'], 'Name'),
      'singular_name' => __( $this->data['singular_name'], 'Singular name' )
    );    

    // Now register the taxonomy
    register_taxonomy($this->data['key'], array(), array(
      'hierarchical' => true,
      'labels' => $labels,
      'show_ui' => true,
      'show_in_rest' => true,
      'show_admin_column' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => $this->data['slug'] ),
    ));

  }
}
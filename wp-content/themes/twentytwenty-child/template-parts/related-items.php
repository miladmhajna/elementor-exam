<?php

$query = new WP_Query(
  [
      'post_type'      => $args['post_type'],
      'posts_per_page' => $args['per_page'],
      'tax_query'      => $args['tax_query'],
      'post__not_in'   => $args['post__not_in'],
      'post_status'    => $args['post_status'],
      'orderby'        => $args['orderby'],
      'order'          => $args['order'],
  ]
);

if(!$query->have_posts())
 return;

?>


<div>
  <h3><?php _e( 'Related items', 'twentytwenty' ) ?></h3>


  <div class="items-grid">
    <?php 
     while ( $query->have_posts() ) : $query->the_post(); ?>
      <a class="item" href="<?php the_permalink($post->ID) ?>" style="background-image: url(<?php the_post_thumbnail_url($post->ID) ?>)">

        <?php if(get_post_meta($post->ID, 'product_on_sale', true) == 'yes') 
          echo "<span class='badge badge-sale'>Sale</span>";
        ?>

        <h3 class="item-title"><?php the_title(); ?></h3>
      </a>

    <?php 
      endwhile; 
      wp_reset_query();  
    ?> 
  </div>

</div>





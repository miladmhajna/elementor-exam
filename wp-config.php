<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'elementor_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '@2020Mah' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'g5s0|_PwD{FLVJsOeB+?X:t/N@8+9###9;ju``s@fb/Enn1Rbjiy%fBmgG~DD`k[');
define('SECURE_AUTH_KEY',  'vhN.39J~AY/v4Ms9V+&e>p?-/SoY0h*S;-*j4jfq^-HK-R^a^E#P#?{l-:=LkG/o');
define('LOGGED_IN_KEY',    'N&26+eI!p]v#wGM8zhU|l^tX?A<&Pe.u]~vFj:g,62Ux,Qp+7_:)W4-k-:+lgTNn');
define('NONCE_KEY',        'jZRpRW0~9=E0uH=e56:Y4aR/(oOo;*CVh|*=@bpT}GASpatPo)Y>$5u=9ORRdW@u');
define('AUTH_SALT',        '*_nJ&z>]M@teD_V=GR|y{&+a72CFl8=yg$Nmk|Qqh]+m+pnsp@SZ|UkEDZh*wcu=');
define('SECURE_AUTH_SALT', 'YY<)-+1nFpf|Oz|yMKJ8ClnmWolS4GF0>doOA7P`8f=vYHma5~xOGXj%S`y/%v#|');
define('LOGGED_IN_SALT',   'w+W#(|MNZbRa.YYgpGDS]|Vs;UX(.umXDnT6gy}_FYas$B=5M{zEEMOt7*<VhnBx');
define('NONCE_SALT',       'M@*yWWHlR}3wUbwu+,hIEKovH[_S9.bek{RM0wEip8/ih0jH+t0+aOryuHi;&:Ps');

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
